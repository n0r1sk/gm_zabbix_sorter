# Greasemonkey Zabbix Hosts Sorter

Sorting hosts via the "Interface" in Zabbix at "Configuration \ Hosts" is a pain.
So we decided to make a script which helps us to get this functionality via Greasemonkey.

Tested with Zabbix 3.4.2
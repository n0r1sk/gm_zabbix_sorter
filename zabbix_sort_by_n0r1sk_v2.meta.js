// ==UserScript==
// @name        Zabbix-Sort by n0r1sk
// @namespace   n0r1sk
// @include     http*://*zabbix*/hosts.php*
// @downloadURL http://wiki.n0r1sk.com/_NON_WIKI_CONTENT/zabbix/zabbix_sort_by_n0r1sk_v2.user.js
// @updateURL http://wiki.n0r1sk.com/_NON_WIKI_CONTENT/zabbix/zabbix_sort_by_n0r1sk_v2.meta.js
// @description Zabbix Host Sort Script by n0r1sk!
// @version     2.6
// @grant       none
// ==/UserScript==
